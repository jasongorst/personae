require "application_system_test_case"

class CharactersTest < ApplicationSystemTestCase
  setup do
    @character = characters(:one)
  end

  test "visiting the index" do
    visit characters_url
    assert_selector "h1", text: "Characters"
  end

  test "creating a Character" do
    visit characters_url
    click_on "New Character"

    fill_in "Description", with: @character.description
    fill_in "Kith kinain", with: @character.kith_kinain
    fill_in "Location", with: @character.location
    fill_in "Mortal name", with: @character.mortal_name
    fill_in "Player", with: @character.player_id
    fill_in "Position", with: @character.position
    fill_in "Rank", with: @character.rank
    fill_in "Use name", with: @character.use_name
    click_on "Create Character"

    assert_text "Character was successfully created"
    click_on "Back"
  end

  test "updating a Character" do
    visit characters_url
    click_on "Edit", match: :first

    fill_in "Description", with: @character.description
    fill_in "Kith kinain", with: @character.kith_kinain
    fill_in "Location", with: @character.location
    fill_in "Mortal name", with: @character.mortal_name
    fill_in "Player", with: @character.player_id
    fill_in "Position", with: @character.position
    fill_in "Rank", with: @character.rank
    fill_in "Use name", with: @character.use_name
    click_on "Update Character"

    assert_text "Character was successfully updated"
    click_on "Back"
  end

  test "destroying a Character" do
    visit characters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Character was successfully destroyed"
  end
end
