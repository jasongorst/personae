import { defineStore } from "pinia"
import { isPositive } from "@/lib/utils"
import { FlashMessage } from "@/lib/FlashMessage"

export const useFlashStore = defineStore( "flash", {
  state: () => ({
    messages: new Map(),
    nextId: 0
  }),

  getters: {
    hasMessages: (state) => isPositive(state.messages.size),
    messageCount: (state) => state.messages.size
  },

  actions: {
    getMessage(id) {
      return this.message.get(id)
    },
    addMessage(text, severity = "notice", isDismissible = true ) {
      const message = new FlashMessage(text, severity, isDismissible)
      this.messages.set(this.nextId, message)
      this.nextId++
    },
    removeMessage(id) {
      this.messages.delete(id)
    },
    removeAllMessages() {
      this.$reset()
    }
  }
})
