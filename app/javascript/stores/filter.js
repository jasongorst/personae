import { defineStore } from "pinia"
import config from "@/lib/config"
import { isEmpty } from "@/lib/utils"

export const useFilterStore = defineStore("filter", {
  state: () => ({
    attributes: Object.fromEntries(
      config.filterAttributes.map(
        (attribute) => [attribute, []]
      )
    )
  }),

  getters: {
    isFilterSet: (state) => {
      for (const attribute of config.filterAttributes) {
        if (!isEmpty(state.attributes[attribute])) {
          return true
        }
      }

      return false
    }
  },

  actions: {
    resetFilter() {
      this.$reset()
    }
  }
})
