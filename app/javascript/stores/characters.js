import { defineStore } from "pinia"
import config from "@/lib/config"
import { isEmpty } from "@/lib/utils"
import { useFilterStore } from "@/stores/filter"

// true if each filterAttribute of character are in attributes[attribute], or attributes[attribute] is empty
function isInFilter(character, attributes) {
  for (const attribute of config.filterAttributes) {
    if (!isEmpty(attributes[attribute]) &&
      !attributes[attribute].includes(character[attribute])) {
      return false
    }
  }

  return true
}

// index of first record on currentPage
function startIndex(currentPage, perPage) {
  return (perPage * (currentPage - 1))
}

// index of last record on currentPage
// i.e., lesser of index of first record on next page and totalItems, minus 1
function endIndex(currentPage, perPage, totalItems) {
  return Math.min(startIndex(currentPage + 1, perPage), totalItems) - 1
}

export const useCharactersStore = defineStore("characters", {
  state: () => ({
    characterList: [],
    currentPage: 1,
    perPage: 12
  }),

  getters: {
    filteredCharacters: (state) => {
      const filter = useFilterStore()

      return state.characterList.filter(
        (character) => isInFilter(character, filter.attributes)
      )
    },

    totalCharacters() {
      return this.filteredCharacters.length
    },

    pagedCharacters() {
      return this.filteredCharacters
        .slice(
          startIndex(this.currentPage, this.perPage),
          // add one to endIndex because javascript's Array.slice() is stupid
          endIndex(this.currentPage, this.perPage, this.totalCharacters) + 1
        )
    }
  },

  actions: {
    sortCharacterList(comparison) {
      this.characterList.sort(comparison)
    }
  }
})
