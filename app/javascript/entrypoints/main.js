import "@/stylesheets/main.css"

import { createApp } from "vue"
import { createPinia } from "pinia"
import App from "@/components/App.vue"

import { library } from "@fortawesome/fontawesome-svg-core"
import { faCircleUser as farCircleUser } from "@fortawesome/free-regular-svg-icons"
import {
  faArrowDownAZ,
  faArrowDownZA,
  faBars,
  faCircleInfo,
  faCircleUser as fasCircleUser,
  faExternalLinkAlt,
  faMasksTheater,
  faSearch,
  faTriangleExclamation,
  faXmark
} from "@fortawesome/free-solid-svg-icons"

library.add(faArrowDownAZ)
library.add(faArrowDownZA)
library.add(faBars)
library.add(faCircleInfo)
library.add(faExternalLinkAlt)
library.add(faMasksTheater)
library.add(faSearch)
library.add(faTriangleExclamation)
library.add(faXmark)
library.add(farCircleUser)
library.add(fasCircleUser)

const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.mount("#app")
