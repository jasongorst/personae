import "@hotwired/turbo-rails"
import "@rails/actiontext"
import * as ActiveStorage from "@rails/activestorage"
import mrujs from "mrujs"
import "trix"

ActiveStorage.start()
mrujs.start()

import { library, dom } from "@fortawesome/fontawesome-svg-core"
import { faCircleUser as farCircleUser } from "@fortawesome/free-regular-svg-icons"
import {
  faBars,
  faCircleInfo,
  faCircleUser as fasCircleUser,
  faExternalLinkAlt,
  faMasksTheater,
  faSearch,
  faTriangleExclamation,
  faXmark
} from "@fortawesome/free-solid-svg-icons"

library.add(faBars)
library.add(faCircleInfo)
library.add(faExternalLinkAlt)
library.add(faMasksTheater)
library.add(faSearch)
library.add(faTriangleExclamation)
library.add(faXmark)
library.add(farCircleUser)
library.add(fasCircleUser)
dom.watch()
