// JsFromRoutes CacheKey daf2cef5a2f96327b538ba6779d562e9
//
// DO NOT MODIFY: This file was automatically generated by JsFromRoutes.
import { definePathHelper } from '@js-from-routes/client'

export default {
  list: definePathHelper('get', '/characters'),
  create: definePathHelper('post', '/characters'),
  new: definePathHelper('get', '/characters/new'),
  edit: definePathHelper('get', '/characters/:id/edit'),
  get: definePathHelper('get', '/characters/:id'),
  update: definePathHelper('patch', '/characters/:id'),
  destroy: definePathHelper('delete', '/characters/:id'),
  search: definePathHelper('get', '/search/:q'),
}
