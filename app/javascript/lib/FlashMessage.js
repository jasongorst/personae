export class FlashMessage {
  constructor(text, severity = "notice", isDismissible = true) {
    this.text = text
    this.severity = severity
    this.isDismissible = isDismissible
  }
}
