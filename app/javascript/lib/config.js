const config = {
  filterAttributes: [
    "player",
    "kith",
    "seeming",
    "location"
  ],
  listAttributes: [
    "player",
    "faeName",
    "mortalName",
    "kith",
    "seeming",
    "rank",
    "location",
    "position"
  ],
  detailAttributes: [
    "player",
    "faeName",
    "mortalName",
    "kith",
    "house",
    "bannerhouse",
    "seeming",
    "rank",
    "location",
    "position"
  ],
  richTextAttributes: [
    "description",
    "notes"
  ]
}

export default config
