// https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
// noinspection AssignmentToFunctionParameterJS,PointlessBitwiseExpressionJS,MagicNumberJS,AssignmentToForLoopParameterJS,JSUnusedGlobalSymbols,AssignmentResultUsedJS

export function cyrb128(str) {
  let h1 = 1779033703, h2 = 3144134277,
    h3 = 1013904242, h4 = 2773480762

  for (let i = 0, k; i < str.length; i++) {
    k = str.charCodeAt(i)
    h1 = h2 ^ Math.imul(h1 ^ k, 597399067)
    h2 = h3 ^ Math.imul(h2 ^ k, 2869860233)
    h3 = h4 ^ Math.imul(h3 ^ k, 951274213)
    h4 = h1 ^ Math.imul(h4 ^ k, 2716044179)
  }

  h1 = Math.imul(h3 ^ (h1 >>> 18), 597399067)
  h2 = Math.imul(h4 ^ (h2 >>> 22), 2869860233)
  h3 = Math.imul(h1 ^ (h3 >>> 17), 951274213)
  h4 = Math.imul(h2 ^ (h4 >>> 19), 2716044179)

  return [(h1^h2^h3^h4)>>>0, (h2^h1)>>>0, (h3^h1)>>>0, (h4^h1)>>>0]
}

export function sfc32(a, b, c, d) {
  a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0
  let t = (a + b) | 0
  a = b ^ b >>> 9
  b = c + (c << 3) | 0
  c = (c << 21 | c >>> 11)
  d = d + 1 | 0
  t = t + d | 0
  c = c + t | 0

  return (t >>> 0) / 4294967296
}

export function mulberry32(a) {
  let t = a += 0x6D2B79F5
  t = Math.imul(t ^ t >>> 15, t | 1)
  t ^= t + Math.imul(t ^ t >>> 7, t | 61)

  return ((t ^ t >>> 14) >>> 0) / 4294967296
}

export function xoshiro128ss(a, b, c, d) {
  let t = b << 9, r = b * 5; r = (r << 7 | r >>> 25) * 9
  c ^= a; d ^= b
  b ^= c; a ^= d; c ^= t
  d = d << 11 | d >>> 21

  return (r >>> 0) / 4294967296
}

export function jsf32(a, b, c, d) {
  a |= 0; b |= 0; c |= 0; d |= 0
  let t = a - (b << 27 | b >>> 5) | 0
  a = b ^ (c << 17 | c >>> 15)
  b = c + d | 0
  c = d + t | 0
  d = a + t | 0

  return (d >>> 0) / 4294967296
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
export function randomInt(min, max, rand) {
  // the maximum is exclusive and the minimum is inclusive
  return Math.floor(rand * (Math.floor(max) - Math.ceil(min)) + Math.ceil(min))
}

export function randomIntInclusive(min, max, rand) {
  // the maximum is inclusive and the minimum is inclusive
  return Math.floor(rand * (Math.floor(max) - Math.ceil(min) + 1) + Math.ceil(min))
}
