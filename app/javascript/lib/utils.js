// array of unique values in array
export function unique(array) {
  // create a Set from the array, then an array from the set
  return Array.from(new Set(array))
}

// true if value is "set" (not null or undefined) and "empty" (as defined by type)
// NOTE: numbers, bigints, booleans, and symbols are never empty
export function isEmpty(value) {
  switch (type(value)) {
    case "string":
      return (value === "")
    case "Array":
      return (value.length === 0)
    case "Set":
    case "Map":
      return (value.size === 0)
    default:
      return false
  }
}

// true if element is null
export function isNull(element) {
  return (element === null)
}

// true if element is undefined (typeof avoids throwing a ReferenceError)
export function isUndefined(element) {
  return (typeof element === "undefined")
}

// true if element is defined and neither null nor empty
export function isPresent(element) {
  return (!isUndefined(element) && !isNull(element) && !isEmpty(element))
}

export function isBlank(element) {
  return !isPresent(element)
}

export function isPositive(number) {
  return (number > 0)
}

export function isZero(number) {
  return (number === 0)
}

export function type(value) {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
  if (value === null) {
    return "null"
  }

  const baseType = typeof value

  // Primitive types
  if (!["object", "function"].includes(baseType)) {
    return baseType
  }

  // Symbol.toStringTag often specifies the "display name" of the
  // object's class. It's used in Object.prototype.toString().
  const tag = value[Symbol.toStringTag]
  if (typeof tag === "string") {
    return tag
  }

  // If it's a function whose source code starts with the "class" keyword
  if (
    baseType === "function" &&
    Function.prototype.toString.call(value).startsWith("class")
  ) {
    return "class"
  }

  // The name of the constructor; for example `Array`, `GeneratorFunction`,
  // `Number`, `String`, `Boolean` or `MyCustomClass`
  const className = value.constructor.name
  if (typeof className === "string" && className !== "") {
    return className
  }

  // At this point there's no robust way to get the type of value,
  // so we use the base implementation.
  return baseType
}

export function confirm(prompt, url) {
  if (window.confirm(prompt)) {
    window.open(url)
  }
}
