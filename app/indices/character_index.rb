ThinkingSphinx::Index.define :character, with: :real_time do
  indexes fae_name, sortable: true
  indexes mortal_name, sortable: true
  indexes kith, sortable: true
  indexes house, sortable: true
  indexes bannerhouse, sortable: true
  indexes seeming, sortable: true
  indexes rank, sortable: true
  indexes location, sortable: true
  indexes position, sortable: true
  indexes description_plain_text, as: :description
  indexes notes_plain_text, as: :notes

  has created_at, type: :timestamp
  has updated_at, type: :timestamp
end
