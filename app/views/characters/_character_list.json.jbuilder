json.extract! character, :id, :player, :fae_name, :mortal_name, :kith, :seeming, :rank, :location, :position

json.url character_url(character, format: :json)
