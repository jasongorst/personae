json.extract! character, :id, :player, :fae_name, :mortal_name, :kith, :house, :bannerhouse, :seeming, :rank, :location, :position

json.description character.description.body
json.notes character.notes.body

json.url character_url(character, format: :json)
