module CharactersHelper
  def options_for_datalist(field)
    Character.group(field).
              order(field).
              pluck(field).
              filter { |f| f.present? }
  end
end
