module MailerHelper
  def url_from_path(path)
    path = "/#{path}" unless path.start_with?("/")

    URI::HTTP.build(
      Rails.application.config.action_mailer.default_url_options.merge({ path: path } )
    ).to_s
  end
end
