require "administrate/base_dashboard"

class AccountDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    email: Field::String,
    status: Field::Select,
    admin: Field::Boolean,
    password_hash: Field::Password,
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    email
    status
    admin
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    email
    status
    admin
  ].freeze

  FORM_ATTRIBUTES = %i[
    email
    password_hash
    status
    admin
  ].freeze

  FORM_ATTRIBUTES_EDIT = %i[
    email
    status
    admin
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  def display_resource(account)
    account.email
  end
end
