class ApplicationMailer < ActionMailer::Base
  helper MailerHelper

  default from: "admin@evilpaws.org"
  layout "mailer"
end
