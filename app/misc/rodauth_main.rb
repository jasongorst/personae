require "sequel/core"

class RodauthMain < Rodauth::Rails::Auth
  configure do
    # List of authentication features that are loaded.
    enable :create_account, :verify_account, :login, :logout, :jwt,
           :reset_password, :change_password, :change_password_notify

    # See the Rodauth documentation for the list of available config options:
    # http://rodauth.jeremyevans.net/documentation.html

    # ==> General
    # Initialize Sequel and have it reuse Active Record's database connection.
    db Sequel.mysql2(extensions: :activerecord_connection, keep_reference: false)

    # Change prefix of table and foreign key column names from default "account"
    # accounts_table :users
    # verify_account_table :user_verification_keys
    # verify_login_change_table :user_login_change_keys
    # reset_password_table :user_password_reset_keys

    # The secret key used for hashing public-facing tokens for various features.
    # Defaults to Rails `secret_key_base`, but you can use your own secret key.
    # hmac_secret "febfbb498df903a544bf3f3b7f459c70d4cfe6912b972bfcb9c5ad3ba171e634144bf9d5649e712a79ab867cba1a8b4f3195552f6e3df476fb93b9ee6c964780"

    # Set JWT secret, which is used to cryptographically protect the token.
    jwt_secret Rails.application.credentials.jwt_secret!

    # Accept only JSON requests.
    only_json? false

    # Handle login and password confirmation fields on the client side.
    # require_password_confirmation? false
    # require_login_confirmation? false

    # Use path prefix for all routes.
    # prefix "/auth"

    # Specify the controller used for view rendering, CSRF, and callbacks.
    rails_controller { RodauthController }

    # Set in Rodauth controller instance with the title of the current page.
    title_instance_variable :@page_title

    # Store account status in an integer column without foreign key constraint.
    account_status_column :status

    # Store password hash in a column instead of a separate table.
    account_password_hash_column :password_hash

    # Do not ask for password when creating or verifying account.
    verify_account_set_password? false
    create_account_set_password? false

    # Prevent the admin from being logged in after confirming the account.
    verify_account_autologin? false

    # Change some default param keys.
    login_param "email"
    # password_confirm_param "confirm_password"

    # Redirect back to originally requested location after authentication.
    login_return_to_requested_location? true
    # two_factor_auth_return_to_requested_location? true # if using MFA

    # Autologin the user after they have reset their password.
    reset_password_autologin? true

    # Delete the account record when the user has closed their account.
    # delete_account_on_close? true

    # Redirect to the app from login and registration pages if already logged in.
    # login_redirect { rails_routes.root_path }
    # already_logged_in { redirect login_redirect }

    # Redirect to root after logout.
    logout_redirect { rails_routes.root_path }

    # ==> Emails
    email_from "admin@evilpaws.org"
    email_subject_prefix "personae.evilpaws.org - "

    verify_account_email_subject "New User Awaiting Admin Approval"
    reset_password_email_subject "Reset Your Password (or set one for your new account)"
    password_changed_email_subject "Password Changed"

    # Use a custom mailer for delivering authentication emails.
    create_reset_password_email do
      RodauthMailer.reset_password(self.class.configuration_name, account_id, reset_password_key_value)
    end
    create_verify_account_email do
      RodauthMailer.verify_account(self.class.configuration_name, account_id, verify_account_key_value)
    end
    # create_verify_login_change_email do |_login|
    #   RodauthMailer.verify_login_change(self.class.configuration_name, account_id, verify_login_change_key_value)
    # end
    create_password_changed_email do
      RodauthMailer.password_changed(self.class.configuration_name, account_id)
    end
    # create_reset_password_notify_email do
    #   RodauthMailer.reset_password_notify(self.class.configuration_name, account_id)
    # end
    # create_email_auth_email do
    #   RodauthMailer.email_auth(self.class.configuration_name, account_id, email_auth_key_value)
    # end
    # create_unlock_account_email do
    #   RodauthMailer.unlock_account(self.class.configuration_name, account_id, unlock_account_key_value)
    # end
    send_email do |email|
      # queue email delivery on the mailer after the transaction commits
      db.after_commit { email.deliver_now }
    end

    # ==> Form params
    login_label "Email Address"
    login_button "Sign In"
    logout_button "Sign Out"
    create_account_link_text "Create a New Account"
    reset_password_request_link_text "Forgot Password?"
    reset_password_explanatory_text "Enter your email address to be emailed a link to reset your password."

    # ==> Flash
    # Override default flash messages.
    # create_account_notice_flash "Your account has been created. Please verify your account by visiting the confirmation link sent to your email address."
    # require_login_error_flash "Login is required for accessing this page"
    login_notice_flash "You are now signed in."
    login_error_flash "There was a problem signing you in."
    logout_notice_flash "You have been signed out."
    reset_password_email_sent_notice_flash "An email has been sent with instructions to reset your password."
    reset_password_notice_flash "Your password has been reset."
    change_password_notice_flash "Your password has been changed."
    attempt_to_login_to_unverified_account_error_flash "The account you tried to login with is awaiting verification."

    # Display this message to the user after they've created their account.
    verify_account_email_sent_notice_flash "Your account has been created and is awaiting approval."

    # Display this message after confirming account.
    verify_account_notice_flash "The account has been approved."

    # ==> Validation
    # Override default validation error messages.
    # no_matching_login_message "user with this email address doesn't exist"
    # already_an_account_with_this_login_message "user with this email address already exists"
    # password_too_short_message { "needs to have at least #{password_minimum_length} characters" }
    # login_does_not_meet_requirements_message { "invalid email#{", #{login_requirement_message}" if login_requirement_message}" }

    # Passwords shorter than 8 characters are considered weak according to OWASP.
    password_minimum_length 8
    # bcrypt has a maximum input length of 72 bytes, truncating any extra bytes.
    password_maximum_bytes 72

    # Custom password complexity requirements (alternative to password_complexity feature).
    # password_meets_requirements? do |password|
    #   super(password) && password_complex_enough?(password)
    # end
    # auth_class_eval do
    #   def password_complex_enough?(password)
    #     return true if password.match?(/\d/) && password.match?(/[^a-zA-Z\d]/)
    #     set_password_requirement_error_message(:password_simple, "requires one number and one special character")
    #     false
    #   end
    # end

    # ==> Hooks
    # Validate custom fields in the create account form.
    # before_create_account do
    #   throw_error_status(422, "name", "must be present") if param("name").empty?
    # end

    # Perform additional actions after the account is created.
    # after_create_account do
    #   Profile.create!(account_id: account_id, name: param("name"))
    # end

    # Do additional cleanup after the account is closed.
    # after_close_account do
    #   Profile.find_by!(account_id: account_id).destroy
    # end

    # Send a reset password email after verifying the account.
    # This allows the user to choose the password for the account,
    # and also makes sure the user can only log in if they have
    # access to the email address for the account.
    after_verify_account do
      generate_reset_password_key_value
      create_reset_password_key
      send_reset_password_email
    end

    # ==> Deadlines
    # Change default deadlines for some actions.
    # verify_account_grace_period 3.days.to_i
    # reset_password_deadline_interval Hash[hours: 6]
    # verify_login_change_deadline_interval Hash[days: 2]
  end
end
