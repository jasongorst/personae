class CharactersController < ApplicationController
  before_action :require_account, only: [:new, :edit, :update, :destroy]
  before_action :set_character, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html do
        # @characters = Character.page params[:page]
        render layout: "vue", action: :vue
      end

      format.json do
        @characters = Character.all
        render :index
      end
    end
  end

  def show; end

  def new
    @character = Character.new
  end

  def edit; end

  def create
    @character = Character.new(character_params)

    respond_to do |format|
      if @character.save
        format.html { redirect_to @character, notice: 'Character was successfully added.' }
        format.json { render :show, status: :created, location: @character }
      else
        format.html { render :new }
        format.json { render json: @character.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @character.update(character_params)
        format.html { redirect_to @character, notice: 'Character was successfully updated.' }
        format.json { render :show, status: :ok, location: @character }
      else
        format.html { render :edit }
        format.json { render json: @character.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @character.destroy
    respond_to do |format|
      format.html { redirect_to characters_url, notice: 'Character was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def search
    if params[:q].present?
      respond_to do |format|
        format.html do
          render layout: "vue", action: :vue
        end

        format.json do
          @characters = Character.search params[:q],
                                         per_page: Character.count,
                                         max_matches: Character.count

          render :index
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to root_url }
        format.json { render status: :unprocessable_entity }
      end
    end
  end

  private

  def require_account
    rodauth.require_account
  end

  def set_character
    @character = Character.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def character_params
    params.require(:character).permit(:mortal_name, :fae_name, :rank, :kith,
                                      :location, :position, :player,
                                      :seeming, :house, :bannerhouse,
                                      :description, :notes)
  end
end
