class Character < ApplicationRecord
  has_rich_text :description
  has_rich_text :notes

  validates :player, presence: true

  ThinkingSphinx::Callbacks.append(
    self, behaviours: [:real_time]
  )

  def description_plain_text
    description.body&.to_plain_text
  end

  def notes_plain_text
    notes.body&.to_plain_text
  end
end
