module.exports = {
  content: [
    "./public/*.html",
    "./app/assets/stylesheets/**/*.{css,scss}",
    "./app/helpers/**/*.rb",
    "./app/javascript/**/*.{vue,js,jsx}",
    "./app/views/**/*.{erb,haml,html,slim}"
  ],
  plugins: [
    require("daisyui"),
    require("@tailwindcss/typography")
  ],
  darkMode: "media",
  daisyui: {
    themes: ["fantasy", "business"],
    darkTheme: "business",
    log: false
  }
}
