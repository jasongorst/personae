# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

CSV.foreach('db/csv/personae.csv', headers: true) do |row|
  Character.create(
    player: row['player'],
    status: row['status'],
    fae_name: row['fae_name'],
    mortal_name: row['mortal_name'],
    kith: row['kith'],
    house: row['house'],
    bannerhouse: row['bannerhouse'],
    seeming: row['seeming'],
    rank: row['rank'],
    location: row['location'],
    position: row['position'],
    description: row['description'].nil? ? nil : "<div>#{row['description'].gsub(/\n/, '<br>')}</div>",
    notes: row['notes'].nil? ? nil : "<div>#{row['notes'].gsub(/\n/, '<br>')}</div>"
  )
end
