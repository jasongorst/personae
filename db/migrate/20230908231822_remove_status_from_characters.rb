class RemoveStatusFromCharacters < ActiveRecord::Migration[7.0]
  def change
    remove_column :characters, :status, :string
  end
end
