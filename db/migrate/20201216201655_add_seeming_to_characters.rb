class AddSeemingToCharacters < ActiveRecord::Migration[6.0]
  def change
    add_column :characters, :seeming, :string
  end
end
