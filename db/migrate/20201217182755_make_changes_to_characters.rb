class MakeChangesToCharacters < ActiveRecord::Migration[6.0]
  def change
    change_table :characters do |t|
      t.rename :use_name, :fae_name
      t.rename :kith_kinain, :kith
      t.string :status
      t.string :house
      t.string :bannerhouse
      t.text :notes
    end
  end
end
