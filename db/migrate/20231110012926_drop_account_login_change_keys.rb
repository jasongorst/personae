class DropAccountLoginChangeKeys < ActiveRecord::Migration[7.1]
  def up
    drop_table :account_login_change_keys
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
