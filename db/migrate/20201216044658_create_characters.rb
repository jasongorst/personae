class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters do |t|
      t.string :mortal_name
      t.string :use_name
      t.string :rank
      t.string :kith_kinain
      t.text :description
      t.string :location
      t.string :position
      t.belongs_to :player, null: false, foreign_key: true

      t.timestamps
    end
  end
end
