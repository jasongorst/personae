class DropDescriptionAndNotesFromCharacters < ActiveRecord::Migration[6.1]
  def change
    remove_column :characters, :description, :text
    remove_column :characters, :notes, :text
  end
end
