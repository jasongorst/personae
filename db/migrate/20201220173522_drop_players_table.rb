class DropPlayersTable < ActiveRecord::Migration[6.1]
  def up
    remove_foreign_key :characters, :players
    remove_column :characters, :player_id
    add_column :characters, :player, :string

    drop_table :players
  end

  def down
    create_table :players do |t|
      t.string :player_name

      t.timestamps
    end

    add_column :characters, :player_id, :integer, null: false
    remove_column :characters, :player
    add_foreign_key :characters, :players
  end
end
