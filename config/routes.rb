Rails.application.routes.draw do
  root to: "characters#index"

  # shadow rodauth routes for js-from-routes generator
  defaults export: true do
    controller :rodauth do
      post "change-password", action: :change_password
      post "create-account", action: :create_account
      post "login", action: :login
      post "logout", action: :logout
      post "reset-password", action: :reset_password
      post "reset-password-request", action: :reset_password_request
      post "verify-account", action: :verify_account
    end
  end

  namespace :admin do
    root to: "accounts#index"
    resources :accounts
  end

  resources :characters, export: true

  get "search", to: "characters#search", as: :search
  get "search/:q", to: "characters#search", export: true
end
